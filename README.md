ansible-snmpd
=====

Configure snmpd on Debian/Ubuntu based systems. Would probably be easy to rewrite for EL/RHEL/Alma and such distros.

Role Variables
--------------

snmpd_secret: This is your secret token
snmpd_cidr: Where the snmp v2 requests will come from
snmpd_location: Descriptive text, where it is housed
snmpd_contact: Also descriptive text, who owns the machine

Example Playbook
----------------

    - hosts: all
      roles:
        - role: snmpd
          snmpd_secret: myawesomesnmpdstring
          snmpd_cidr: 127.0.0.1/32
          snmpd_location: MyLocation
          snmpd_contact: Admin <admin@localhost""

License
-------

MIT
